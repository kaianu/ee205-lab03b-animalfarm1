///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
struct catDB {
   char name[30];
   enum Gender gender;
   enum CatBreeds catBreeds;
   bool isFixed;
   float weight;
   enum Color collar1_color;
   enum Color collar2_color;
   long license;
}

/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {
   strcpy(alice.name, "Alice");
   alice.gender = FEMALE;
   alice.catBreeds = MAIN_COON;
   alice.isFixed = true;
   alice.weight = 12.34;
   alice.collar1_color = BLACK;
   alice.collar2_color = RED;
   alice.license = 12345;
  
}
/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   // Here's a clue of what one printf() might look like... 
   printf ("Cat name = [%s]\n", catName( catDB[i].name ));
   printf ("\tgender = [%s]\n", catGender( catDB[i].gender ));
   printf ("\tisFixed = [%s]\n", tf( catDB[i].isFixed ));
   printf ("\tweight = [%f]\n", ( catDB[i].weight ));
   printf ("\tcollar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
   printf ("\tcollar color 2 = [%s]\n", colorName( catDB[i].collar2_color ));
   printf ("\tlicense = [%d]\n", ( catDB[i].license ));
}

