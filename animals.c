///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"
/// Print a cat from the database
///
/// @param int i Which cat in the database that should be printed
/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {
   
   switch(Color){
      case BLACK:
         return "Black";
      case WHITE:
         return "White";
      case RED:
         return "Red";
      case BLUE:
         return "Blue";
      case GREEN:
         return "Green";
      case PINK:
         return "Pink";
         
   }
   // @todo Map the enum Color to a string

 return NULL; // We should never get here
};


