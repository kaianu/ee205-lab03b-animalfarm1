###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

all: animalfarm

animalfarm: main.o animals.o cat.o
	gcc -o animalfarm *.c
	$(info You need to write your own Makefile)
	$(info I know you can do it)
	$(info for now type gcc -o animalfarm *.c)

main.c: animals.h cat.h

animals.c: animals.h cat.h

cat.c: animals.h cat.h

clean:
	rm -f *.o animalfarm
